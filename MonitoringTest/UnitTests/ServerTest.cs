using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerSimulation;

namespace MonitoringTest
{
    [TestClass]
    public class ServerTest
    {
        [TestMethod]
        public void SpeicherplatzErhoehenTest()
        {
            Server testServer = new Server();
            double temp = testServer.BelegterSpeicherplatz;
            testServer.BelegeSpeicherplatz();
            Assert.IsTrue(testServer.BelegterSpeicherplatz > temp);
        }

        [TestMethod]
        public void ArbeitsspeicherauslastungErhoehenTest()
        {
            Server testServer = new Server();
            double temp = testServer.AuslastungArbeitsspeicher;
            testServer.StarteProzess();
            Assert.IsTrue(testServer.AuslastungArbeitsspeicher > temp);
        }

        [TestMethod]
        public void ProzesseBeendenVerringertArbeitsspeicherAuslastungTest()
        {
            Server testServer = new Server();
            double temp = testServer.AuslastungArbeitsspeicher;
            testServer.BeendeProzess();
            Assert.IsTrue(testServer.AuslastungArbeitsspeicher < temp);
        }
    }
}
