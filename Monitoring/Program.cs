﻿using System.Windows.Forms;
using ServerSimulation;
using System.Timers;
using Monitoring;

namespace LF8_MonitoringSoftware
{
    class Program 
    {

        static void Main()
        {
            var server1 = new Server("Hildegard");
            var server2 = new Server("Günther-Jochen");
            var server3 = new Server("Moin Moin");
            var monitor1 = new Monitor(server1);
            var monitor2 = new Monitor(server2);
            var monitor3 = new Monitor(server3);

            var gui = new ServerGui(server1, server2, server3);

            var ServerTimer = new System.Timers.Timer(500);

            ServerTimer.Elapsed += server1.TueZufaelligeServerSachenEvent;
            ServerTimer.Elapsed += monitor1.PruefeMesswerteEvent;
            ServerTimer.Elapsed += server2.TueZufaelligeServerSachenEvent;
            ServerTimer.Elapsed += monitor2.PruefeMesswerteEvent;
            ServerTimer.Elapsed += server3.TueZufaelligeServerSachenEvent;
            ServerTimer.Elapsed += monitor3.PruefeMesswerteEvent;
            ServerTimer.Elapsed += gui.ServerGui_Update;
            ServerTimer.AutoReset = true;
            ServerTimer.Enabled = true;

            // Nur zu Test- & Demonstrationszwecken so; sollte automatisch in Log schreiben, wenn passiert.
            server1.LoggeUserEin("Testuser");
            monitor1.SchreibeInLogLogin("Testuser", true);
            server2.LoggeUserAus("Ich");
            monitor2.SchreibeInLogLogin("Ich", false);

            Application.Run(gui);
        }

    }
 
   
}
