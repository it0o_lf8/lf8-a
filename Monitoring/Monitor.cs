﻿using ServerSimulation;
using System;
using System.IO;
using System.Timers;

namespace Monitoring
{
    //Konstruktor
    public class Monitor
    {
        public Monitor(Server server) : this(server, 75, 90, 150, 200, 80, 95)
        {
        }

        public Monitor(Server server, double speicherSoftLimit, double speicherHardLimit, int prozesseSoftLimit, int prozesseHardLimit, double arbeitsspeicherSoftLimit, double arbeitsspeicherHardLimit)
        {
            Server = server;
            SpeicherlimitSoft = speicherSoftLimit;
            SpeicherlimitHard = speicherHardLimit;
            ProzesslimitSoft = prozesseSoftLimit;
            ProzesslimitHard = prozesseHardLimit;
            ArbeitsspeicherlimitSoft = arbeitsspeicherSoftLimit;
            ArbeitsspeicherlimitHard = arbeitsspeicherHardLimit;
        }

        public Server Server { get; private set; }

        // Soft Limits
        public double SpeicherlimitSoft { get; private set; } // in %
        public int ProzesslimitSoft { get; private set; }
        public double ArbeitsspeicherlimitSoft { get; private set; } // in %

        // Hard Limits
        public double SpeicherlimitHard { get; private set; } // in %
        public int ProzesslimitHard { get; private set; }
        public double ArbeitsspeicherlimitHard { get; private set; } // in %

        public void PruefeMesswerteEvent(object source, ElapsedEventArgs e)
        {
            if (IstSchwellenwertErreichtSoft())
            {
                SchreibeInLogMesswerte();
                return;
            }

            if (IstSchwellenwertErreichtHard())
            {
                // Evtl. E-Mail Benachrichtigung hier, oder Alarm per Ton o. Ä.
                SchreibeInLogMesswerte();
                return;
            }

        }

        //Methode, die Auf dem Desktop einen Ordner und eine .txt Datei erstellt und diese mit den Abfragewerten und der aktuellen Uhrzeit füllt.
        private void SchreibeInLogMesswerte()
        {
            string folderName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string docPath = System.IO.Path.Combine(folderName, "Logs");
            Directory.CreateDirectory(docPath);
            //Falls der Ordner bereits vorhanden ist, führt CreateDirectory keine Aktion aus, und es wird keine Ausnahme ausgelöst.

            DateTime thisDate = DateTime.Now;

            StreamWriter sw = new StreamWriter(Path.Combine(docPath, "log.txt"), true);

            //Die Ausgaben werden gerundet für bessere Lesbarkeit
            sw.Write($"{thisDate:g} Warnung (Server {Server.Name}): Belegter Speicherplatz: {Math.Round(Server.BelegterSpeicherplatz, 2)} GB ({Math.Round(Server.BelegterSpeicherplatz / Server.Speicherplatz * 100, 0)}%)\n");
            sw.Write($"{thisDate:g} Warnung (Server {Server.Name}): Laufende Prozesse: {Server.AnzahlLaufendeProzesse} \n");
            sw.Write($"{thisDate:g} Warnung (Server {Server.Name}): Auslastung Arbeitsspeicher: {Math.Round(Server.AuslastungArbeitsspeicher, 2)} GB ({Math.Round(Server.AuslastungArbeitsspeicher / Server.Arbeitsspeicher * 100, 0)}%)\n");

            sw.Close();
        }

        public void SchreibeInLogLogin(String username, bool logInOderOut) // 0 = LogIn, 1 = LogOut
        {
            string folderName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string docPath = System.IO.Path.Combine(folderName, "Logs");
            Directory.CreateDirectory(docPath);
            //Falls der Ordner bereits vorhanden ist, führt CreateDirectory keine Aktion aus, und es wird keine Ausnahme ausgelöst.

            DateTime thisDate = DateTime.Now;

            StreamWriter sw = new StreamWriter(Path.Combine(docPath, "log.txt"), true);

            if (logInOderOut)
            {
                sw.Write($"{thisDate:g} Log In (Server {Server.Name}): {username} hat sich eingeloggt.\n");
            }
            else
            {
                sw.Write($"{thisDate:g} Log Out (Server {Server.Name}): {username} hat sich ausgeloggt.\n");
            }
           
            sw.Close();
        }

        private bool IstSchwellenwertErreichtSoft()
        {
            if (Math.Round(Server.BelegterSpeicherplatz / Server.Speicherplatz * 100, 0) > SpeicherlimitSoft)
            {
                return true;
            }

            if (Server.AnzahlLaufendeProzesse > ProzesslimitSoft)
            {
                return true;
            }

            if (Math.Round(Server.AuslastungArbeitsspeicher / Server.Arbeitsspeicher * 100, 0) > ArbeitsspeicherlimitSoft)
            {
                return true;
            }

            return false;
        }

        private bool IstSchwellenwertErreichtHard()
        {
            if (Math.Round(Server.BelegterSpeicherplatz / Server.Speicherplatz, 0) >= SpeicherlimitHard)
            {
                return true;
            }

            if (Server.AnzahlLaufendeProzesse >= ProzesslimitHard)
            {
                return true;
            }

            if (Math.Round(Server.AuslastungArbeitsspeicher / Server.Arbeitsspeicher, 0) >= ArbeitsspeicherlimitHard)
            {
                return true;
            }

            return false;
        }

    }
}

