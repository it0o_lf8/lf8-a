﻿using System;
using System.Timers;
using System.Windows.Forms;

namespace ServerSimulation
{
    public partial class ServerGui : Form
    {
        Server ErsterServer { get; set; }
        Server ZweiterServer { get; set; }
        Server DritterServer { get; set; }

        public ServerGui(Server server1, Server server2, Server server3)
        {
            ErsterServer = server1;
            ZweiterServer = server2;
            DritterServer = server3;
            InitializeComponent();
        }

        private void ServerGui_Load(object sender, EventArgs e)
        {
            // Server 1
            Server1Box.Text = "Server 1 - " + ErsterServer.Name;
            Server1SpeicherBar.Maximum = ErsterServer.Speicherplatz;
            Server1SpeicherBar.Value = (int)ErsterServer.BelegterSpeicherplatz;
            Server1Speicher.Text = Math.Round(ErsterServer.BelegterSpeicherplatz, 3).ToString() + " GB/" + ErsterServer.Speicherplatz.ToString() + "GB";
            Server1Prozesse.Text = ErsterServer.AnzahlLaufendeProzesse.ToString();
            Server1ArbeitsspeicherBar.Maximum = ErsterServer.Arbeitsspeicher;
            Server1ArbeitsspeicherBar.Value = (int)ErsterServer.AuslastungArbeitsspeicher;
            Server1Arbeitsspeicher.Text = Math.Round(ErsterServer.AuslastungArbeitsspeicher, 3).ToString() + " GB/" + ErsterServer.Arbeitsspeicher.ToString() + "GB";

            // Server 2
            Server2Box.Text = "Server 2 - " + ZweiterServer.Name;
            Server2SpeicherBar.Maximum = ZweiterServer.Speicherplatz;
            Server2SpeicherBar.Value = (int)ZweiterServer.BelegterSpeicherplatz;
            Server2Speicher.Text = Math.Round(ZweiterServer.BelegterSpeicherplatz, 3).ToString() + " GB/" + ZweiterServer.Speicherplatz.ToString() + "GB";
            Server2Prozesse.Text = ZweiterServer.AnzahlLaufendeProzesse.ToString();
            Server2ArbeitsspeicherBar.Maximum = ZweiterServer.Arbeitsspeicher;
            Server2ArbeitsspeicherBar.Value = (int)ZweiterServer.AuslastungArbeitsspeicher;
            Server2Arbeitsspeicher.Text = Math.Round(ZweiterServer.AuslastungArbeitsspeicher, 3).ToString() + " GB/" + ZweiterServer.Arbeitsspeicher.ToString() + "GB";

            // Server 3
            Server3Box.Text = "Server 3 - " + DritterServer.Name;
            Server3SpeicherBar.Maximum = DritterServer.Speicherplatz;
            Server3SpeicherBar.Value = (int)DritterServer.BelegterSpeicherplatz;
            Server3Speicher.Text = Math.Round(DritterServer.BelegterSpeicherplatz, 3).ToString() + " GB/" + DritterServer.Speicherplatz.ToString() + "GB";
            Server3Prozesse.Text = DritterServer.AnzahlLaufendeProzesse.ToString();
            Server3ArbeitsspeicherBar.Maximum = DritterServer.Arbeitsspeicher;
            Server3ArbeitsspeicherBar.Value = (int)DritterServer.AuslastungArbeitsspeicher;
            Server3Arbeitsspeicher.Text = Math.Round(DritterServer.AuslastungArbeitsspeicher, 3).ToString() + " GB/" + DritterServer.Arbeitsspeicher.ToString() + "GB";
        }

        public void ServerGui_Update(object source, ElapsedEventArgs e)
        {
            ParUpdate();
        }

        private void ParUpdate()
        {
            Invoke(new MethodInvoker(delegate ()
            {
                // Server 1
                Server1SpeicherBar.Value = (int)ErsterServer.BelegterSpeicherplatz;
                Server1Speicher.Text = Math.Round(ErsterServer.BelegterSpeicherplatz, 3).ToString() + " GB/" + ErsterServer.Speicherplatz.ToString() + "GB";
                Server1Prozesse.Text = ErsterServer.AnzahlLaufendeProzesse.ToString();
                Server1ArbeitsspeicherBar.Value = (int)ErsterServer.AuslastungArbeitsspeicher;
                Server1Arbeitsspeicher.Text = Math.Round(ErsterServer.AuslastungArbeitsspeicher, 3).ToString() + " GB/" + ErsterServer.Arbeitsspeicher.ToString() + "GB";

                // Server 2
                Server2SpeicherBar.Value = (int)ZweiterServer.BelegterSpeicherplatz;
                Server2Speicher.Text = Math.Round(ZweiterServer.BelegterSpeicherplatz, 3).ToString() + " GB/" + ZweiterServer.Speicherplatz.ToString() + "GB";
                Server2Prozesse.Text = ZweiterServer.AnzahlLaufendeProzesse.ToString();
                Server2ArbeitsspeicherBar.Value = (int)ZweiterServer.AuslastungArbeitsspeicher;
                Server2Arbeitsspeicher.Text = Math.Round(ZweiterServer.AuslastungArbeitsspeicher, 3).ToString() + " GB/" + ZweiterServer.Arbeitsspeicher.ToString() + "GB";

                // Server 3
                Server3SpeicherBar.Value = (int)DritterServer.BelegterSpeicherplatz;
                Server3Speicher.Text = Math.Round(DritterServer.BelegterSpeicherplatz, 3).ToString() + " GB/" + DritterServer.Speicherplatz.ToString() + "GB";
                Server3Prozesse.Text = DritterServer.AnzahlLaufendeProzesse.ToString();
                Server3ArbeitsspeicherBar.Value = (int)DritterServer.AuslastungArbeitsspeicher;
                Server3Arbeitsspeicher.Text = Math.Round(DritterServer.AuslastungArbeitsspeicher, 3).ToString() + " GB/" + DritterServer.Arbeitsspeicher.ToString() + "GB";
            }));
        }
    }
}
