﻿
namespace ServerSimulation
{
    partial class ServerGui
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Server1Box = new System.Windows.Forms.GroupBox();
            this.Server1Arbeitsspeicher = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Server1ArbeitsspeicherBar = new System.Windows.Forms.ProgressBar();
            this.Server1Prozesse = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Server1Speicher = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Server1SpeicherBar = new System.Windows.Forms.ProgressBar();
            this.Server2Box = new System.Windows.Forms.GroupBox();
            this.Server2Arbeitsspeicher = new System.Windows.Forms.Label();
            this.Server2Prozesse = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Server2ArbeitsspeicherBar = new System.Windows.Forms.ProgressBar();
            this.label5 = new System.Windows.Forms.Label();
            this.Server2Speicher = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Server2SpeicherBar = new System.Windows.Forms.ProgressBar();
            this.Server3Box = new System.Windows.Forms.GroupBox();
            this.Server3Arbeitsspeicher = new System.Windows.Forms.Label();
            this.Server3Prozesse = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Server3ArbeitsspeicherBar = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.Server3Speicher = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Server3SpeicherBar = new System.Windows.Forms.ProgressBar();
            this.ServerLogBox = new System.Windows.Forms.GroupBox();
            this.MonitoringLog = new System.Windows.Forms.Label();
            this.Server1Box.SuspendLayout();
            this.Server2Box.SuspendLayout();
            this.Server3Box.SuspendLayout();
            this.ServerLogBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Server1Box
            // 
            this.Server1Box.Controls.Add(this.Server1Arbeitsspeicher);
            this.Server1Box.Controls.Add(this.label8);
            this.Server1Box.Controls.Add(this.Server1ArbeitsspeicherBar);
            this.Server1Box.Controls.Add(this.Server1Prozesse);
            this.Server1Box.Controls.Add(this.label4);
            this.Server1Box.Controls.Add(this.Server1Speicher);
            this.Server1Box.Controls.Add(this.label1);
            this.Server1Box.Controls.Add(this.Server1SpeicherBar);
            this.Server1Box.Location = new System.Drawing.Point(11, 10);
            this.Server1Box.Name = "Server1Box";
            this.Server1Box.Size = new System.Drawing.Size(234, 234);
            this.Server1Box.TabIndex = 0;
            this.Server1Box.TabStop = false;
            // 
            // Server1Arbeitsspeicher
            // 
            this.Server1Arbeitsspeicher.AutoSize = true;
            this.Server1Arbeitsspeicher.Location = new System.Drawing.Point(7, 207);
            this.Server1Arbeitsspeicher.Name = "Server1Arbeitsspeicher";
            this.Server1Arbeitsspeicher.Size = new System.Drawing.Size(60, 15);
            this.Server1Arbeitsspeicher.TabIndex = 7;
            this.Server1Arbeitsspeicher.Text = "0 GB/0 GB";
            this.Server1Arbeitsspeicher.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(6, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 20);
            this.label8.TabIndex = 6;
            this.label8.Text = "Arbeitsspeicher";
            // 
            // Server1ArbeitsspeicherBar
            // 
            this.Server1ArbeitsspeicherBar.ForeColor = System.Drawing.Color.Lime;
            this.Server1ArbeitsspeicherBar.Location = new System.Drawing.Point(6, 181);
            this.Server1ArbeitsspeicherBar.Name = "Server1ArbeitsspeicherBar";
            this.Server1ArbeitsspeicherBar.Size = new System.Drawing.Size(222, 23);
            this.Server1ArbeitsspeicherBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Server1ArbeitsspeicherBar.TabIndex = 5;
            // 
            // Server1Prozesse
            // 
            this.Server1Prozesse.AutoSize = true;
            this.Server1Prozesse.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Server1Prozesse.Location = new System.Drawing.Point(155, 116);
            this.Server1Prozesse.Name = "Server1Prozesse";
            this.Server1Prozesse.Size = new System.Drawing.Size(17, 20);
            this.Server1Prozesse.TabIndex = 4;
            this.Server1Prozesse.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(6, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Laufende Prozesse:";
            // 
            // Server1Speicher
            // 
            this.Server1Speicher.AutoSize = true;
            this.Server1Speicher.Location = new System.Drawing.Point(7, 79);
            this.Server1Speicher.Name = "Server1Speicher";
            this.Server1Speicher.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Server1Speicher.Size = new System.Drawing.Size(60, 15);
            this.Server1Speicher.TabIndex = 2;
            this.Server1Speicher.Text = "0 GB/0 GB";
            this.Server1Speicher.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Speicherplatz";
            // 
            // Server1SpeicherBar
            // 
            this.Server1SpeicherBar.ForeColor = System.Drawing.Color.Lime;
            this.Server1SpeicherBar.Location = new System.Drawing.Point(6, 53);
            this.Server1SpeicherBar.Name = "Server1SpeicherBar";
            this.Server1SpeicherBar.Size = new System.Drawing.Size(222, 23);
            this.Server1SpeicherBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Server1SpeicherBar.TabIndex = 0;
            // 
            // Server2Box
            // 
            this.Server2Box.Controls.Add(this.Server2Arbeitsspeicher);
            this.Server2Box.Controls.Add(this.Server2Prozesse);
            this.Server2Box.Controls.Add(this.label9);
            this.Server2Box.Controls.Add(this.Server2ArbeitsspeicherBar);
            this.Server2Box.Controls.Add(this.label5);
            this.Server2Box.Controls.Add(this.Server2Speicher);
            this.Server2Box.Controls.Add(this.label2);
            this.Server2Box.Controls.Add(this.Server2SpeicherBar);
            this.Server2Box.Location = new System.Drawing.Point(262, 10);
            this.Server2Box.Name = "Server2Box";
            this.Server2Box.Size = new System.Drawing.Size(234, 234);
            this.Server2Box.TabIndex = 1;
            this.Server2Box.TabStop = false;
            // 
            // Server2Arbeitsspeicher
            // 
            this.Server2Arbeitsspeicher.AutoSize = true;
            this.Server2Arbeitsspeicher.Location = new System.Drawing.Point(6, 207);
            this.Server2Arbeitsspeicher.Name = "Server2Arbeitsspeicher";
            this.Server2Arbeitsspeicher.Size = new System.Drawing.Size(60, 15);
            this.Server2Arbeitsspeicher.TabIndex = 10;
            this.Server2Arbeitsspeicher.Text = "0 GB/0 GB";
            this.Server2Arbeitsspeicher.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Server2Prozesse
            // 
            this.Server2Prozesse.AutoSize = true;
            this.Server2Prozesse.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Server2Prozesse.Location = new System.Drawing.Point(155, 116);
            this.Server2Prozesse.Name = "Server2Prozesse";
            this.Server2Prozesse.Size = new System.Drawing.Size(17, 20);
            this.Server2Prozesse.TabIndex = 5;
            this.Server2Prozesse.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(6, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 20);
            this.label9.TabIndex = 9;
            this.label9.Text = "Arbeitsspeicher";
            // 
            // Server2ArbeitsspeicherBar
            // 
            this.Server2ArbeitsspeicherBar.ForeColor = System.Drawing.Color.Lime;
            this.Server2ArbeitsspeicherBar.Location = new System.Drawing.Point(6, 181);
            this.Server2ArbeitsspeicherBar.Name = "Server2ArbeitsspeicherBar";
            this.Server2ArbeitsspeicherBar.Size = new System.Drawing.Size(222, 23);
            this.Server2ArbeitsspeicherBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Server2ArbeitsspeicherBar.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(6, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Laufende Prozesse:";
            // 
            // Server2Speicher
            // 
            this.Server2Speicher.AutoSize = true;
            this.Server2Speicher.Location = new System.Drawing.Point(6, 79);
            this.Server2Speicher.Name = "Server2Speicher";
            this.Server2Speicher.Size = new System.Drawing.Size(60, 15);
            this.Server2Speicher.TabIndex = 3;
            this.Server2Speicher.Text = "0 GB/0 GB";
            this.Server2Speicher.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(6, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Speicherplatz";
            // 
            // Server2SpeicherBar
            // 
            this.Server2SpeicherBar.ForeColor = System.Drawing.Color.Lime;
            this.Server2SpeicherBar.Location = new System.Drawing.Point(6, 53);
            this.Server2SpeicherBar.Name = "Server2SpeicherBar";
            this.Server2SpeicherBar.Size = new System.Drawing.Size(222, 23);
            this.Server2SpeicherBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Server2SpeicherBar.TabIndex = 1;
            // 
            // Server3Box
            // 
            this.Server3Box.Controls.Add(this.Server3Arbeitsspeicher);
            this.Server3Box.Controls.Add(this.Server3Prozesse);
            this.Server3Box.Controls.Add(this.label11);
            this.Server3Box.Controls.Add(this.Server3ArbeitsspeicherBar);
            this.Server3Box.Controls.Add(this.label6);
            this.Server3Box.Controls.Add(this.Server3Speicher);
            this.Server3Box.Controls.Add(this.label3);
            this.Server3Box.Controls.Add(this.Server3SpeicherBar);
            this.Server3Box.Location = new System.Drawing.Point(514, 10);
            this.Server3Box.Name = "Server3Box";
            this.Server3Box.Size = new System.Drawing.Size(234, 234);
            this.Server3Box.TabIndex = 2;
            this.Server3Box.TabStop = false;
            // 
            // Server3Arbeitsspeicher
            // 
            this.Server3Arbeitsspeicher.AutoSize = true;
            this.Server3Arbeitsspeicher.Location = new System.Drawing.Point(6, 207);
            this.Server3Arbeitsspeicher.Name = "Server3Arbeitsspeicher";
            this.Server3Arbeitsspeicher.Size = new System.Drawing.Size(60, 15);
            this.Server3Arbeitsspeicher.TabIndex = 13;
            this.Server3Arbeitsspeicher.Text = "0 GB/0 GB";
            this.Server3Arbeitsspeicher.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Server3Prozesse
            // 
            this.Server3Prozesse.AutoSize = true;
            this.Server3Prozesse.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Server3Prozesse.Location = new System.Drawing.Point(155, 116);
            this.Server3Prozesse.Name = "Server3Prozesse";
            this.Server3Prozesse.Size = new System.Drawing.Size(17, 20);
            this.Server3Prozesse.TabIndex = 6;
            this.Server3Prozesse.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label11.Location = new System.Drawing.Point(6, 156);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 20);
            this.label11.TabIndex = 12;
            this.label11.Text = "Arbeitsspeicher";
            // 
            // Server3ArbeitsspeicherBar
            // 
            this.Server3ArbeitsspeicherBar.ForeColor = System.Drawing.Color.Lime;
            this.Server3ArbeitsspeicherBar.Location = new System.Drawing.Point(6, 181);
            this.Server3ArbeitsspeicherBar.Name = "Server3ArbeitsspeicherBar";
            this.Server3ArbeitsspeicherBar.Size = new System.Drawing.Size(222, 23);
            this.Server3ArbeitsspeicherBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Server3ArbeitsspeicherBar.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(6, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Laufende Prozesse:";
            // 
            // Server3Speicher
            // 
            this.Server3Speicher.AutoSize = true;
            this.Server3Speicher.Location = new System.Drawing.Point(6, 79);
            this.Server3Speicher.Name = "Server3Speicher";
            this.Server3Speicher.Size = new System.Drawing.Size(60, 15);
            this.Server3Speicher.TabIndex = 4;
            this.Server3Speicher.Text = "0 GB/0 GB";
            this.Server3Speicher.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(6, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Speicherplatz";
            // 
            // Server3SpeicherBar
            // 
            this.Server3SpeicherBar.ForeColor = System.Drawing.Color.Lime;
            this.Server3SpeicherBar.Location = new System.Drawing.Point(6, 53);
            this.Server3SpeicherBar.Name = "Server3SpeicherBar";
            this.Server3SpeicherBar.Size = new System.Drawing.Size(222, 23);
            this.Server3SpeicherBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Server3SpeicherBar.TabIndex = 2;
            // 
            // ServerLogBox
            // 
            this.ServerLogBox.Controls.Add(this.MonitoringLog);
            this.ServerLogBox.Location = new System.Drawing.Point(12, 246);
            this.ServerLogBox.Name = "ServerLogBox";
            this.ServerLogBox.Size = new System.Drawing.Size(735, 119);
            this.ServerLogBox.TabIndex = 3;
            this.ServerLogBox.TabStop = false;
            this.ServerLogBox.Text = "Server Log";
            // 
            // MonitoringLog
            // 
            this.MonitoringLog.AutoSize = true;
            this.MonitoringLog.Location = new System.Drawing.Point(6, 19);
            this.MonitoringLog.Name = "MonitoringLog";
            this.MonitoringLog.Size = new System.Drawing.Size(93, 15);
            this.MonitoringLog.TabIndex = 0;
            this.MonitoringLog.Text = "Server Log Kram";
            // 
            // ServerGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 373);
            this.Controls.Add(this.ServerLogBox);
            this.Controls.Add(this.Server3Box);
            this.Controls.Add(this.Server2Box);
            this.Controls.Add(this.Server1Box);
            this.Name = "ServerGui";
            this.Text = "Server-Monitoring";
            this.Load += new System.EventHandler(this.ServerGui_Load);
            this.Server1Box.ResumeLayout(false);
            this.Server1Box.PerformLayout();
            this.Server2Box.ResumeLayout(false);
            this.Server2Box.PerformLayout();
            this.Server3Box.ResumeLayout(false);
            this.Server3Box.PerformLayout();
            this.ServerLogBox.ResumeLayout(false);
            this.ServerLogBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Server1Box;
        private System.Windows.Forms.GroupBox Server2Box;
        private System.Windows.Forms.GroupBox Server3Box;
        private System.Windows.Forms.ProgressBar Server1SpeicherBar;
        private System.Windows.Forms.Label Server1Speicher;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar Server2SpeicherBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar Server3SpeicherBar;
        private System.Windows.Forms.Label Server1Arbeitsspeicher;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ProgressBar Server1ArbeitsspeicherBar;
        private System.Windows.Forms.Label Server1Prozesse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Server2Arbeitsspeicher;
        private System.Windows.Forms.Label Server2Prozesse;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ProgressBar Server2ArbeitsspeicherBar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Server2Speicher;
        private System.Windows.Forms.Label Server3Arbeitsspeicher;
        private System.Windows.Forms.Label Server3Prozesse;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar Server3ArbeitsspeicherBar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Server3Speicher;
        private System.Windows.Forms.GroupBox ServerLogBox;
        private System.Windows.Forms.Label MonitoringLog;
    }
}

