﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace ServerSimulation
{
    // Soll einen Server simulieren.
    public class Server
    {
        // Konstruktoren
        /// <summary>
        /// Erstellt eine neue Instanz eines Servers mit den Standardwerten für den Namen, den Speicherplatz und den verfügbaren Arbeitsspeicher.
        /// </summary>
        public Server() : this("Unbenannter Server", 1000, 8)
        {
        }
        /// <summary>
        /// Erstellt eine neue Instanz eines Servers mit den Standardwerten für den Speicherplatz und den verfügbaren Arbeitsspeicher.
        /// </summary>
        /// <param name="name">Name des Servers</param>
        public Server(String name) : this(name, 1000, 8)
        {
        }
        /// <summary>
        /// Erstellt eine neue Instanz eines Servers mit bestimmten Werten für den Speicherplatz und den verfügbaren Arbeitsspeicher.
        /// </summary>
        /// <param name="name">Name des Servers</param>
        /// <param name="gesamtSpeicher">Verfügbarer Gesamtspeicherplatz in GB</param>
        /// <param name="gesamtArbeitsspeicher">Verfügbarer Gesamtarbeitsspeicher in GB</param>
        public Server(String name, int gesamtSpeicher, int gesamtArbeitsspeicher)
        {
            Name = name;
            var rand = new Random();
            Speicherplatz = gesamtSpeicher;
            BelegterSpeicherplatz = gesamtSpeicher / 4;
            AnzahlLaufendeProzesse = 3;
            Arbeitsspeicher = gesamtArbeitsspeicher;
            AuslastungArbeitsspeicher = AnzahlLaufendeProzesse * rand.NextDouble();
            EingeloggteUser = new List<string>();
        }


        // Eigenschaften
        // Name des Servers
        public String Name { get; set; }
        // Insgesamt vorhandener Speicherplatz auf dem Server, in GB
        public int Speicherplatz { get; private set; }

        // Belegter Speicherplatz auf dem Server in GB
        public double BelegterSpeicherplatz { get; private set; }

        // Anzahl laufender Prozesse auf dem Server
        public int AnzahlLaufendeProzesse { get; private set; }

        //Insgesamt vorhandener Arbeitsspeicher auf dem Server, in GB
        public int Arbeitsspeicher { get; private set; }

        // Auslastung des Arbeitsspeichers in GB
        public double AuslastungArbeitsspeicher { get; private set; }

        // Liste aller derzeit eingeloggten User (Username)
        public List<String> EingeloggteUser { get; private set; }


        // Methoden

        public void TueZufaelligeServerSachenEvent(object source, ElapsedEventArgs e)
        {
            var rand = new Random();

            int auswahl = rand.Next(0, 4);

            switch (auswahl)
            {
                case 0:
                    return;
                case 1:
                    BelegeSpeicherplatz();
                    return;
                case 2:
                    GebeSpeicherplatzFrei();
                    return;
                case 3:
                    StarteProzess();
                    return;
                case 4:
                    BeendeProzess();
                    return;
            }
        }

        /// <summary>
        /// Belegt eine zufällig generierte Menge an GB des Speicherplatzes.
        /// </summary>
        public void BelegeSpeicherplatz()
        {
            var rand = new Random();

            BelegeSpeicherplatz(rand.NextDouble() * 100);
        }

        /// <summary>
        /// Belegt eine bestimmte Menge an GB des Speicherplatzes.
        /// </summary>
        /// <param name="zuBelegenderSpeicher">Zu belegender Speicherplatz in GB</param>
        public void BelegeSpeicherplatz(double zuBelegenderSpeicher)
        {
            if (zuBelegenderSpeicher + BelegterSpeicherplatz > Speicherplatz)
            {
                BelegterSpeicherplatz = Speicherplatz;
                return;
            }

            BelegterSpeicherplatz += zuBelegenderSpeicher;
        }

        /// <summary>
        /// Gibt eine zufällig generierte Menge an GB des Speicherplatzes wieder frei.
        /// </summary>
        public void GebeSpeicherplatzFrei()
        {
            var rand = new Random();

            GebeSpeicherplatzFrei(rand.NextDouble() * 100);
        }

        /// <summary>
        /// Gibt eine bestimmte Menge an GB des Speicherplatzes wieder frei.
        /// </summary>
        /// <param name="freizugebenderSpeicher">Freizugebender Speicherplatz in GB</param>
        public void GebeSpeicherplatzFrei(double freizugebenderSpeicher)
        {
            if (BelegterSpeicherplatz - freizugebenderSpeicher < 0)
            {
                BelegterSpeicherplatz = 0;
                return;
            }

            BelegterSpeicherplatz -= freizugebenderSpeicher;
        }

        /// <summary>
        /// Startet eine zufällige Anzahl an Prozessen und erhöht auch die Auslastung des Arbeitsspeichers dementsprechend.
        /// </summary>
        public void StarteProzess()
        {
            var rand = new Random();

            StarteProzess(rand.Next(1, 10));
        }

        /// <summary>
        /// Startet eine bestimmte Anzahl an Prozessen und erhöht auch die Auslastung des Arbeitsspeichers dementsprechend.
        /// </summary>
        /// <param name="anzahl">Anzahl der zu startenden Prozesse</param>
        public void StarteProzess(int anzahl)
        {
            if (anzahl <= 0)
            {
                return;
            }

            AnzahlLaufendeProzesse += anzahl;
            ErhoeheArbeitsspeicherauslastung(anzahl);
        }

        /// <summary>
        /// Beendet eine zufällige Anzahl von Prozessen und verringert auch die Auslastung des Arbeitsspeichers dementsprechend.
        /// </summary>
        public void BeendeProzess()
        {
            var rand = new Random();

            BeendeProzess(rand.Next(1, AnzahlLaufendeProzesse));
        }

        /// <summary>
        /// Beendet eine bestimmte Anzahl von Prozessen und verringert auch die Auslastung des Arbeitsspeichers dementsprechend.
        /// </summary>
        /// <param name="anzahl">Anzahl der zu beendenden Prozesse</param>
        public void BeendeProzess(int anzahl)
        {
            if (anzahl <= 0)
            {
                return;
            }

            AnzahlLaufendeProzesse -= anzahl;
            VerringereArbeitsspeicherauslastung(anzahl);
        }

        /// <summary>
        /// Erhöht die Auslastung des Arbeitsspeichers entsprechend der Anzahl neu gestarteter Prozesse.
        /// </summary>
        /// <param name="anzahlProzesse">Anzahl gestarteter Prozesse</param>
        private void ErhoeheArbeitsspeicherauslastung(int anzahlProzesse)
        {
            var rand = new Random();
            AuslastungArbeitsspeicher += anzahlProzesse * rand.NextDouble() / 10;
            if (AuslastungArbeitsspeicher > Arbeitsspeicher)
            {
                AuslastungArbeitsspeicher = Arbeitsspeicher;
            }
        }

        /// <summary>
        /// Verringert die Auslastung des Arbeitsspeichers entsprechend der Anzahl beendeter Prozesse.
        /// </summary>
        /// <param name="anzahlProzesse">Anzahl beendeter Prozesse</param>
        private void VerringereArbeitsspeicherauslastung(int anzahlProzesse)
        {
            var rand = new Random();
            AuslastungArbeitsspeicher -= anzahlProzesse * rand.NextDouble() / 10;
            if (AuslastungArbeitsspeicher < 0)
            {
                AuslastungArbeitsspeicher = 0;
            }
        }

        /// <summary>
        /// Fügt einen sich einloggenden User der Liste der eingeloggten User hinzu.
        /// </summary>
        /// <param name="username">Username des sich einloggenden Users</param>
        public void LoggeUserEin(String username)
        {
            EingeloggteUser.Add(username);
        }

        /// <summary>
        /// Entfernt einen sich ausloggenden User der Liste der eingeloggten User.
        /// </summary>
        /// <param name="username">Username des sich ausloggenden Users</param>
        public void LoggeUserAus(String username)
        {
            EingeloggteUser.Remove(username);
        }
    }
}
